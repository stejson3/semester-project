import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from statistics_computation import computePearson, computeSpearman


def evaluate_test_results(labels, predictions, total_loss):
    """Function to create the test results

    Computation of metrics for test results

    Args:
        labels (ndarray): true values
        predictions (ndarray): predictions
        total_loss (float): total loss computed from loss function in testinf

    Returns:
        dictionary: results of testing
    """

    mse_all = mean_squared_error(labels, predictions)
    rmse_all = np.sqrt(mse_all)

    mse = mean_squared_error(labels, predictions, multioutput="raw_values")
    rmse = np.sqrt(mse)

    results = {
        "MSE_all": mse_all.tolist(),
        "RMSE_all": rmse_all.tolist(),
        "MSE": mse.tolist(),
        "RMSE": rmse.tolist(),
        "total_loss": total_loss,
        "pearsons": computePearson(labels, predictions).tolist(),
        "spearmans": computeSpearman(labels, predictions).tolist(),
    }
    return results


def create_csv_of_labels_predictions(predictions, labels, path, names):
    """Function to create csv of labels and predictions

    Args:
        predictions (_type_): predictions
        labels (_type_): labels
        path (_type_): path to created file
        names (_type_): names of the features
    """
    data_dict = {}
    for i in range(predictions.shape[1]):
        data_dict[f"label_{names[i]}"] = labels[:, i]
        data_dict[f"prediction_{names[i]}"] = predictions[:, i]

    # Create pandas DataFrame
    df = pd.DataFrame(data_dict)
    df.to_csv(path + ".csv", index=False)
