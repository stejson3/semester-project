import os
import torch
import models as md

RESULTS_PATH = os.path.join("..", "results")
MODELS_PATH = os.path.join("..", "models")

ANNOTATION_FEATURES = ["plaque_width", "other_plaque_width"]
ALL_TRANSVERSAL_FEATURES = ["proportion_green_blue","wall_area", "plaque_area","lumen_area", "wall_circle", "plaque_circle", "lumen_circle"]
ALL_LONGITUDAL_FEATURES = ['min_lumen_width', 'max_plaque_width', 'max_ratio', 'avg_lumen_width', 'avg_plaque_width', 'avg_ratio', 'plaque_length', 'proportion_green_blue', 'plaque_circle_diameter', 'wall_area', 'lumen_area', 'plaque_area']
REAL_TRANSVERSAL_FEATURES = ["proportion_green_blue","wall_area","lumen_area","plaque_area","max_plaque_area"]
REAL_LONGITUDAL_FEATURES = ["min_lumen_width","max_plaque_width","max_ratio","avg_lumen_width","avg_plaque_width","avg_ratio","plaque_length","proportion_green_blue","wall_area","lumen_area","plaque_area"]
FIRST_FEATURE = ["proportion_green_blue"]
BATCH_SIZE = 1
WEIGH_DECAY = (10)**(-6)
LEARNING_RATE = 5 * (10)**(-7)
EPOCHS_NUMBER = 10
DEVICE_NUMBER = 0

STANDARD_HYPERPARAMETERS = {
        "batch_size": BATCH_SIZE,
        "weight_decay":  0.00005,
        "learning_rate": 5 * 10**(-6)
    }

################################################### DECIDING WHICH ARCHITECTURE ###################################################

################# Not pretrained
resnet34_segmentation_notpretrained_trainable = {
    "name": "resnet34_segmentation_notpretrained_trainable",
    "description" : "experiment to tests resnet with random initialization on transversal data part of deciding which architecture to use",
    "model_name": "resnet34_segmentation_notpretrained_trainable",
    "training_type": "fully_trainable",
    "pretrained": False,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": FIRST_FEATURE,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "transversal",
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# Pretrained traineble
resnet34_segmentation_pretrained_trainable = {
    "name": "resnet34_segmentation_pretrained_trainable",
    "description" : "experiment to tests resnet with pretrained weights on transversal data part of deciding which architecture to use",
    "model_name": "resnet34_segmentation_pretrained_trainable",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": FIRST_FEATURE,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "transversal",
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# Pretraine feature extractor
resnet34_segmentation_pretrained_freezed = {
    "name": "resnet34_segmentation_pretrained_freezed",
    "description" : "experiment to tests resnet with pretrained weights with freezed resnet on transversal data part of deciding which architecture to use",
    "model_name": "resnet34_segmentation_pretrained_freezed",
    "training_type": "feature_extractor",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": FIRST_FEATURE,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "transversal",
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# FLIP TEST
resnet34_segmentation_pretrained_trainable_flips = {
    "name": "resnet34_segmentation_pretrained_trainable_flips",
    "description" : "experiment to tests resnet with pretrained weights with flips on transversal data part of deciding which architecture to use",
    "model_name": "resnet34_segmentation_pretrained_trainable_flips",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": FIRST_FEATURE,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "transversal",
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": True, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}



################################################### Synthetic transversal ###################################################

################# segmentations
synthTrans_segmentation_resnet = {
    "name": "synthTrans_segmentation_resnet",
    "description" : "experiment on synthetic transversal segmentation all features",
    "model_name": "synthTrans_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ALL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# 1-channel grayscale
synthTrans_ultra1_resnet = {
    "name": "synthTrans_ultra1_resnet",
    "description" : "experiment on synthetic transversal grayscale 1 channel random image all features",
    "model_name": "synthTrans_ultra1_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": False,
        "include_images": True,
        "expand_grayscale": False,
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# 3-channel grayscale
synthTrans_ultra3_resnet = {
    "name": "synthTrans_ultra3_resnet",
    "description" : "experiment on synthetic transversal grayscale 3 channel random image all features",
    "model_name": "synthTrans_ultra3_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ALL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": False,
        "include_images": True,
        "expand_grayscale": True,
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# segmentation + 1 grayscale = 4channel
synthTrans_conc4_resnet = {
    "name": "synthTrans_conc4_resnet",
    "description" : "experiment on synthetic transversal grayscale 1 channel + 3 segmenrtation random image all features",
    "model_name": "synthTrans_conc4_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": True,
        "expand_grayscale": False,
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# segmentation + 3 grayscale = 6channel
synthTrans_conc6_resnet = {
    "name": "synthTrans_conc6_resnet",
    "description" : "experiment on synthetic transversal grayscale 3 channel + 3 segmenrtation random image all features",
    "model_name": "synthTrans_conc6_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": True,
        "expand_grayscale": True,
        "dataset_name": "transversal_5000",
        "dataset_size": 5000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################################################### Synthetic longitudinal ###################################################


################# segmentations
synthLong_segmentation_resnet = {
    "name": "synthLong_segmentation_resnet",
    "description" : "experiment on synthetic longitudinal segmentation all features",
    "model_name": "synthLong_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ALL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": False,
        "dataset_name": "longitudal_10000",
        "dataset_size": 10000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}



################# 1-channel grayscale
synthLong_ultra1_resnet = {
    "name": "synthLong_ultra1_resnet",
    "description" : "experiment on synthetic longitudinal grayscale 1 channel random image all features",
    "model_name": "synthLong_ultra1_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": False,
        "include_images": True,
        "expand_grayscale": False,
        "dataset_name": "longitudal_10000",
        "dataset_size": 10000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# 3-channel grayscale
synthLong_ultra3_resnet = {
    "name": "synthLong_ultra3_resnet",
    "description" : "experiment on synthetic longitudinal grayscale # channel random image all features",
    "model_name": "synthLong_ultra3_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ALL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": False,
        "include_images": True,
        "expand_grayscale": True,
        "dataset_name": "longitudal_10000",
        "dataset_size": 10000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# 4-channel conc
synthLong_conc4_resnet = {
    "name": "synthLong_conc4_resnet",
    "description" : "experiment on synthetic longitudinal concatenated 4 channel sgmentation random image all features",
    "model_name": "synthLong_conc4_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": True,
        "expand_grayscale": False,
        "dataset_name": "longitudal_10000",
        "dataset_size": 10000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}


################# 6-channel conc
synthLong_conc6_resnet = {
    "name": "synthLong_conc6_resnet",
    "description" : "experiment on synthetic longitudinal concatenated 6 channel sgmentation random image all features",
    "model_name": "synthLong_conc6_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ALL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "synth",
        "include_segmentations": True,
        "include_images": True,
        "expand_grayscale": True,
        "dataset_name": "longitudal_10000",
        "dataset_size": 10000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################################################### Real Images - labels from segmentations Transversal ###################################################

################# segmentations
RILSTrans_segmentation_resnet = {
    "name": "RILSTrans_segmentation_resnet",
    "description" : "experiment on segmetnations from real data with labels derived from segmentations from kostelansky, transversal data, only segmentations",
    "model_name": "RILSTrans_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": REAL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "trans",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# ultrasound
RILSTrans_ultrasound_resnet = {
    "name": "RILSTrans_ultrasound_resnet",
    "description" : "experiment on ultrasound real data with labels derived from segmentations from kostelansky, transversal data",
    "model_name": "RILSTrans_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": REAL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": False,
        "include_images": True,
        "image_orientation": "trans",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# conc
RILSTrans_conc_resnet = {
    "name": "RILSTrans_conc_resnet",
    "description" : "experiment on concatenated ultrasound and segmententatio from real data with labels derived from segmentations from kostelansky, transversal data",
    "model_name": "RILSTrans_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": REAL_TRANSVERSAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": True,
        "image_orientation": "trans",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}


################################################### Real Images - labels from segmentations Longitudinal ###################################################

################# segmentations
RILSLong_segmentation_resnet = {
    "name": "RILSLong_segmentation_resnet",
    "description" : "experiment on segmetnations from real data with labels derived from segmentations from kostelansky, longitudinal data, only segmentations",
    "model_name": "RILSLong_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": REAL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "long",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# ultrasound
RILSLong_ultrasound_resnet = {
    "name": "RILSLong_ultrasound_resnet",
    "description" : "experiment on ultrasound real data with labels derived from segmentations from kostelansky, longitudinal data",
    "model_name": "RILSLong_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": REAL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": False,
        "include_images": True,
        "image_orientation": "long",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# conc
RILSLong_conc_resnet = {
    "name": "RILSLong_conc_resnet",
    "description" : "experiment on concatenated ultrasound and segmententatio from real data with labels derived from segmentations from kostelansky, longitudinal data",
    "model_name": "RILSLong_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": REAL_LONGITUDAL_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": True,
        "image_orientation": "long",
        "real_labels": False,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}




















################################################### Real Images - labels from annotations Transversal ###################################################

################# segmentations
AnnotationsTrans_segmentation_resnet = {
    "name": "AnnotationsTrans_segmentation_resnet",
    "description" : "experiment on segmetnations from real data with labels derived from anntotaions by doctors, transversal data, only segmentations",
    "model_name": "AnnotationsTrans_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "trans",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# ultrasound
AnnotationsTrans_ultrasound_resnet = {
    "name": "AnnotationsTrans_ultrasound_resnet",
    "description" : "experiment on ultrasound real data with labels derived from anntotaions by doctors, transversal data",
    "model_name": "AnnotationsTrans_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": False,
        "include_images": True,
        "image_orientation": "trans",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# conc
AnnotationsTrans_conc_resnet = {
    "name": "AnnotationsTrans_conc_resnet",
    "description" : "experiment on concatenated ultrasound and segmententatio from real data with labels derived from anntotaions by doctors, transversal data",
    "model_name": "AnnotationsTrans_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": True,
        "image_orientation": "trans",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}


################################################### Real Images - labels from annotations Longitudinal ###################################################

################# segmentations
AnnotationsLong_segmentation_resnet = {
    "name": "AnnotationsLong_segmentation_resnet",
    "description" : "experiment on segmetnations from real data with labels derived from anntotaions by doctors, longitudinal data, only segmentations",
    "model_name": "AnnotationsLong_segmentation_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": False,
        "image_orientation": "long",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# ultrasound
AnnotationsLong_ultrasound_resnet = {
    "name": "AnnotationsLong_ultrasound_resnet",
    "description" : "experiment on ultrasound real data with labels derived from anntotaions by doctors, longitudinal data",
    "model_name": "AnnotationsLong_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34RGBchannels,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": False,
        "include_images": True,
        "image_orientation": "long",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

################# conc
AnnotationsLong_conc_resnet = {
    "name": "AnnotationsLong_conc_resnet",
    "description" : "experiment on concatenated ultrasound and segmententatio from real data with labels derived from anntotaions by doctors, longitudinal data",
    "model_name": "AnnotationsLong_ultrasound_resnet",
    "training_type": "fully_trainable",
    "pretrained": True,
    "model_function": md.Resnet34DynamicInputDim,
    "predicted_features": ANNOTATION_FEATURES,
    "total_number_of_epochs": 50,
    "loss_function": torch.nn.MSELoss, 
    "optimizer_function": torch.optim.Adam,
    "scheduler":{
        "step_size": 10,
        "gamma": 0.1,
    },
    "dataset":{
        "label_normalization": "normal_normalization",  
        "images_type": "real",
        "include_segmentations": True,
        "include_images": True,
        "image_orientation": "long",
        "real_labels": True,
        "path": os.path.join("..", "data", "real_data"),
        "dataset_size": 20000,
        "train_percentage": 0.8,
        "test_percentage": 0.1,
        "val_percentage": 0.1,
        "transforms": False, 

    },
    "hyperparameters": STANDARD_HYPERPARAMETERS
}

