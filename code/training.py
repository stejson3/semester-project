import time
import os
import numpy as np
import torch
from torch.optim.lr_scheduler import StepLR
import evaluation as ev
from statistics_computation import computePearson, computeSpearman

MODELS_PATH = os.path.join("..", "models")
RESULTS_PATH = os.path.join("..", "results")


def test_multi_input_model(model, config, dataloader, folder_path, device):
    """Function implements testing phase for CNN

    Args:
        model (nn.Module): pytorch model
        config (dictionary): config dictionary
        dataloader (DataLoader): pytorch dataloader
        folder_path (string): path to the save folder
        device (device): pytorch device

    Returns:
        dict: results of the evaluation
    """
    loss_function = config["loss_function"]()

    model.eval()
    all_outs = []
    all_labels = []
    total_loss = 0

    with torch.no_grad():
        for images, labels in dataloader:
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            all_labels.append(labels.cpu().numpy())
            all_outs.append(outputs.cpu().detach().numpy())
            loss = loss_function(outputs, labels)
            total_loss += loss.item()
            torch.cuda.empty_cache()

    all_labels = np.vstack(all_labels)
    all_outs = np.vstack(all_outs)

    result_path = os.path.join(folder_path, f'{config["name"]}_test_results')
    # saving the raw results from the testing
    np.savez(result_path + ".npz", predictions=all_outs, true_values=all_labels)
    # saving the csv results from the testing
    ev.create_csv_of_labels_predictions(
        all_outs, all_labels, result_path, config["predicted_features"]
    )

    return ev.evaluate_test_results(all_labels, all_outs, total_loss=total_loss)


def train_multi_input_model(
    model, config, train_dataloader, val_dataloader, device, folder_path
):
    """Function that implements the training process

    Standard training pytorch loop with validation, the best model is saved as the one with highest pearson coefficient.


    Args:
        model (nn.Module): pytorch model
        config (dictionary): config dictionary
        train_dataloader (DataLoader): pytorch dataloader for training
        val_dataloader (DataLoader): pytorch dataloader for validation
        device (device): pytorch device

    Returns:
        best_model_path (path string): path to the best trained model
        info_dict (dictionary): information dictionary
    """

    loss_function = config["loss_function"]()
    optimizer = config["optimizer_function"](
        params=model.parameters(),
        lr=config["hyperparameters"]["learning_rate"],
        weight_decay=config["hyperparameters"]["weight_decay"],
    )
    num_epochs = config["total_number_of_epochs"]
    model_name = config["model_name"]
    scheduler = StepLR(
        optimizer,
        step_size=config["scheduler"]["step_size"],
        gamma=config["scheduler"]["gamma"],
    )

    training_loss_history = []
    validation_loss_history = []
    pearson_coefs = []
    spearman_coefs = []
    best_epoch = 0
    best_epoch_t_loss = 0
    best_epoch_v_loss = 0
    max_coeff = -np.inf

    since = time.time()
    for epoch in range(num_epochs):
        # ------------------ Training ------------------
        model.train()
        print("-" * 30)
        print(f"Epoch {epoch}/{num_epochs}, lr = {optimizer.param_groups[0]['lr']}")
        epoch_training_loss = 0.0  # loss over whole epoch
        running_loss = 0.0  # loss over 1000 samples
        for i, (images, labels) in enumerate(train_dataloader):
            images = images.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()

            # Forward pass
            outputs = model(images)
            loss = loss_function(outputs, labels)

            # Backward and optimize
            loss.backward()
            optimizer.step()

            curr_loss = loss.item()
            running_loss += curr_loss
            if (i + 1) % 1000 == 999:
                print(f"  batch {i+1} loss: {running_loss/1000:.4f}")
                running_loss = 0.0

            epoch_training_loss += curr_loss
            if i % 50 == 0:
                torch.cuda.empty_cache()

        scheduler.step()
        avg_tloss = epoch_training_loss / len(train_dataloader)
        training_loss_history.append(avg_tloss)

        # ------------------ Validating ------------------
        model.eval()
        epoch_validation_loss = 0.0
        all_labels = []  # all labels from validation
        all_outs = []  # all predictions from validation

        with torch.no_grad():
            for i, (vinputs, vlabels) in enumerate(val_dataloader):
                vinputs = vinputs.to(device)
                vlabels = vlabels.to(device)
                voutputs = model(vinputs)
                vloss = loss_function(voutputs, vlabels)
                epoch_validation_loss += vloss.item()

                all_labels.append(vlabels.cpu().numpy())
                all_outs.append(voutputs.cpu().detach().numpy())
                torch.cuda.empty_cache()

        all_labels = np.vstack(all_labels)
        all_outs = np.vstack(all_outs)
        pearson = computePearson(all_labels, all_outs)
        spearman = computeSpearman(all_labels, all_outs)
        pearson_coefs.append(pearson.tolist())
        spearman_coefs.append(spearman.tolist())

        result_path = os.path.join(folder_path, "validation_results", f"val_{epoch}")

        # creating csv from the validation results
        ev.create_csv_of_labels_predictions(
            all_outs, all_labels, result_path, config["predicted_features"]
        )

        avg_vloss = epoch_validation_loss / len(val_dataloader)
        validation_loss_history.append(avg_vloss)
        print(
            "LOSS train: {:.4f} validation: {:.4f} PearsonSumAVG {:.4f} SpearmanAVG {:.4f}".format(
                avg_tloss,
                avg_vloss,
                np.sum(pearson) / pearson.shape[0],
                np.sum(spearman) / spearman.shape[0],
            )
        )
        print(f"Pearsons: {pearson}")
        print(f"Spearmans: {spearman}")

        # Deciding if this epoch is best based on validation results
        pearson_sum = np.sum(pearson)
        if pearson_sum > max_coeff:
            best_epoch_t_loss = avg_tloss
            best_epoch_v_loss = avg_vloss
            best_pearson = pearson_sum
            best_epoch = epoch
            max_coeff = pearson_sum
            print("Saving Best Model")
            torch.save(
                model.state_dict(), os.path.join(MODELS_PATH, f"{model_name}.pt")
            )

    time_elapsed = time.time() - since
    print(
        "Training complete in {:.0f}m {:.0f}s".format(
            time_elapsed // 60, time_elapsed % 60
        )
    )

    # information saved about trainig part of the experiment
    info_dict = {
        "best_epoch": best_epoch,
        "training_loss": best_epoch_t_loss,
        "validation_loss": best_epoch_v_loss,
        "peason": best_pearson,
        "training_loss_history": training_loss_history,
        "validation_loss_history": validation_loss_history,
        "spearman_hitory": spearman_coefs,
        "pearson_history": pearson_coefs,
        "time_to_train": time_elapsed,
    }

    return os.path.join(MODELS_PATH, f"{model_name}.pt"), info_dict
