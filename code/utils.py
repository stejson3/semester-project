import json
import os
import numpy as np
import pandas as pd
from PIL import Image
from skimage.transform import resize
from compute_metrics import (
    compute_longitudal_geometrical_params,
    compute_transversal_geometrical_params,
)
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from scipy.spatial.distance import cdist


def minmax_normalize_pandas(path, filename, features):
    """create minmax normalized version of the labels

    Args:
        path (str): path to labels
        filename (str): labels filename
        features (lis(str)): which features persist
    """
    df = pd.read_csv(os.path.join(path, filename))
    scaler = MinMaxScaler()
    df[features] = scaler.fit_transform(df[features])
    df.to_csv(os.path.join(path, f"minmax_normalization_{filename}"), index=False)


def normal_normalize_pandas(path, filename, features):
    """create normal normalized version of the labels

    Args:
        path (str): path to labels
        filename (str): labels filename
        features (lis(str)): which features persist
    """
    df = pd.read_csv(os.path.join(path, filename))
    scaler = StandardScaler()
    df[features] = scaler.fit_transform(df[features])
    df.to_csv(os.path.join(path, f"normal_normalization_{filename}"), index=False)


def create_normalized_labels(dataset_name="transversal_3000"):
    """create normalized labels from a dataset based on segmentations

    Args:
        dataset_name (str, optional): path to the dataset. Defaults to "transversal_3000".
    """
    labels_path = os.path.join(
        "..", "data", "synthetic_pictures", dataset_name, "labels"
    )
    raw_labels_path = os.path.join(labels_path, "raw_labels")
    label_files = os.listdir(raw_labels_path)

    features = []
    with open(os.path.join(raw_labels_path, "image_0.json"), "r") as f:
        label_dict = json.load(f)
        features = label_dict.keys()
    print(features)

    arr = np.empty((len(label_files), len(features)))
    for i in range(len(label_files)):
        l_name = f"image_{i}.json"
        f_lab_path = os.path.join(raw_labels_path, l_name)
        with open(f_lab_path, "r") as f:
            label_dict = json.load(f)
        arr[i] = [label_dict[feature] for feature in features]

    mean = np.mean(arr, axis=0)
    std = np.std(arr, axis=0)
    normal_normalized = (arr - mean) / std
    normal_params = {"mean": mean.tolist(), "std": std.tolist()}

    min_vals = np.min(arr, axis=0)
    max_vals = np.max(arr, axis=0)
    minmax_params = {"min": min_vals.tolist(), "max": max_vals.tolist()}

    minmax_normalized = (arr - min_vals) / (max_vals - min_vals)

    df = pd.DataFrame(arr, columns=features)
    df_normal = pd.DataFrame(normal_normalized, columns=features)
    df_minmax = pd.DataFrame(minmax_normalized, columns=features)

    df.to_csv(os.path.join(labels_path, "no_normalization.csv"), index=False)
    df_normal.to_csv(os.path.join(labels_path, "normal_normalization.csv"), index=False)
    df_minmax.to_csv(os.path.join(labels_path, "minmax_normalization.csv"), index=False)

    with open(os.path.join(labels_path, "normal_params.json"), "w") as f:
        json.dump(normal_params, f)
    with open(os.path.join(labels_path, "minmax_params.json"), "w") as f:
        json.dump(minmax_params, f)


def ultra_image_resize(image):
    """Functions that resizes the ultrasound image to the siz 256x256xnumber_of_channels

    Args:
        image (cv2.Image): image

    Returns:
        ndarray: resized image
    """
    new_height = 256
    new_width = 256

    image = resize(np.array(image), (new_height, new_width))

    return image


def seg_image_resize(image):
    """Function to resize image and then replace the interpolated values with the closest full color to maintain the segments

     Args:
        image (cv2.Image): image

    Returns:
        ndarray: resized image
    """
    new_height = 256
    new_width = 256
    image = resize(np.array(image), (new_height, new_width))
    colors = np.array([[255, 0, 0], [0, 255, 0], [0, 0, 255]])
    image_2d = image.reshape(-1, 3)
    distances = cdist(image_2d, colors)
    closest_color_indices = np.argmin(distances, axis=1)
    closest_colors = colors[closest_color_indices]
    closest_colors = closest_colors.reshape(image.shape)
    mask = np.all(image < 0.1, axis=-1)
    closest_colors[mask] = [0, 0, 0]
    return closest_colors


def create_labels_for_resized_transversal_data(data_folder_path):
    """Function that create labels from geometric parameters of the resized transverse images and stores them to csv

    Args:
        data_folder_path (str): path to the folder
    """
    prog_path = os.path.join(data_folder_path, "progressive", "Trans")
    stable_path = os.path.join(data_folder_path, "stable", "Trans")
    data = []
    for fold in [prog_path, stable_path]:
        for i, filename in enumerate(os.listdir(fold)):
            if i % 100 == 0:
                print(i)
            if filename.endswith(".jpg") or filename.endswith(
                ".png"
            ): 
                image_path = os.path.join(fold, filename)

                image = Image.open(image_path)
                image = image.convert("RGB")

                resized_image = seg_image_resize(image)
                row = compute_transversal_geometrical_params(resized_image)
                row["name"] = filename
                row["type"] = "stable" if fold == stable_path else "progressive"
                data.append(row)

    df = pd.DataFrame(data)
    df.to_csv(
        os.path.join(
            data_folder_path,
            "..",
            "labels",
            "resized_segmentation_tansversal_labels.csv",
        )
    )


def create_labels_for_resized_longitudal_data(data_folder_path):
    """Function that create labels from geometric parameters of the resized longitudinal images and stores them to csv

    Args:
        data_folder_path (str): path to the folder
    """
    prog_path = os.path.join(data_folder_path, "progressive", "Long")
    stable_path = os.path.join(data_folder_path, "stable", "Long")
    data = []

    for fold in [prog_path, stable_path]:
        for i, filename in enumerate(os.listdir(fold)):
            if i % 100 == 0:
                print(i)
            if filename.endswith(".jpg") or filename.endswith(
                ".png"
            ):  # Add other image formats if needed
                image_path = os.path.join(fold, filename)

                image = Image.open(image_path)
                image = image.convert("RGB")

                resized_image = seg_image_resize(image)

                row = compute_longitudal_geometrical_params(resized_image)
                row["name"] = filename
                row["type"] = "stable" if fold == stable_path else "progressive"
                data.append(row)
                # break

    # Create a DataFrame from the collected data
    df = pd.DataFrame(data)
    df.to_csv(
        os.path.join(
            data_folder_path,
            "..",
            "labels",
            "resized_segmentation_longitudal_labels.csv",
        )
    )


def intersection_in_data():
    """Function to check if there are any duplicates in stable and progressive dataset
    """
    segmntation_folder = os.path.join("..", "data", "real_data", "masks_resized")
    prog_path = os.path.join(segmntation_folder, "progressive", "Trans")
    stable_path = os.path.join(segmntation_folder, "stable", "Trans")
    attributes_csv = pd.read_csv(
        os.path.join("..", "data", "data_with_attributes.csv"), header=0
    )
    in_both = []
    print(attributes_csv.head())
    for index, row in attributes_csv.iterrows():
        img_name = row["img_name"]
        if row["img_type"] == "Trans" and (
            (
                row["stability"] == "progressive"
                and os.path.exists(os.path.join(prog_path, img_name))
            )
            or (
                row["stability"] == "stable"
                and os.path.exists(os.path.join(stable_path, img_name))
            )
        ):
            in_both.append(img_name)
    print(in_both)
    print(len(in_both))


def create_labels_from_annotations():
    """Create labels from the data_with_attributes.csv file
    """
    attributes_csv = pd.read_csv(
        os.path.join("..", "code", "data_with_attributes.csv"), header=0
    )
    long = []
    trans = []
    for index, row in attributes_csv.iterrows():
        features = {
            "type": "progressive" if row["stability"] != "stable" else "stable",
            "name": row["img_name"],
        }
        for column_name, value in row.iteritems():
            features[column_name] = value
        if row["img_type"] == "Trans":
            trans.append(features)
        else:
            long.append(features)

    df_l = pd.DataFrame(long)
    df_t = pd.DataFrame(trans)
    # Save DataFrame to CSV
    "resized_annotations_{'tansversal' if is_transversal else 'longitudal'}.csv"
    df_l.to_csv(
        os.path.join(
            "..", "data", "real_data", "labels", "resized_annotations_longitudal.csv"
        ),
        index=False,
    )
    df_t.to_csv(
        os.path.join(
            "..", "data", "real_data", "labels", "resized_annotations_transversal.csv"
        ),
        index=False,
    )
