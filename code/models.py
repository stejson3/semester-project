import time
import random
import os
import pandas as pd
import numpy as np
import torch
from torchvision import models
from torch import nn
from torch.optim.lr_scheduler import StepLR
import evaluation as ev
import torch.nn.init as init


class SmallCNN(nn.Module):
    """
    Class implements simple NN accepting 256x256x4 image
    """
    def __init__(self, input_dim = 4, output_dim = 1):
        super(SmallCNN, self).__init__()
        
        self.conv1 = nn.Conv2d(input_dim, 16, kernel_size=7, padding=3)
        self.bn1 = nn.BatchNorm2d(16)
        self.relu1 = nn.ReLU()
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, padding=2)
        self.bn2 = nn.BatchNorm2d(32)
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        self.conv3 = nn.Conv2d(32, 64, kernel_size=3, padding=1)
        self.bn3 = nn.BatchNorm2d(64)
        self.relu3 = nn.ReLU()
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        self.fc1 = nn.Linear(64*32*32, 512)
        self.relu4 = nn.ReLU()
        
        self.fc2 = nn.Linear(512, output_dim)
        
    def forward(self, x):
        # 4 256 256
        x = self.relu1(self.bn1(self.conv1(x)))  # 16 128 128
        x = self.pool1(x) # 16 128 128
        x = self.relu2(self.bn2(self.conv2(x)))
        x = self.pool2(x) # 32 64 64
        x = self.relu3(self.bn3(self.conv3(x)))
        x = self.pool3(x) # 64 32 32
        x = x.reshape(1, 64* 32* 32)

        x = self.relu4(self.fc1(x))
        x = self.fc2(x)
        return x
    
class Resnet34DynamicInputDim(nn.Module):  
    """Resnet neural net for regreesion

    This neural net accepts images of size 256x256 with arbitrary number of channels, the input layer is changed
    and the output classification layer is replaced by fully connected layer with 3 layers with possibility to set arbitrary number of outputs
    also the net can be modified as a feature exctractor or fullly trainable network

    """
    def __init__(self, pretrained = True,input_dim = 3, output_dim = 1, training_type = "fully_trainable"):
        """init method for Resnet34DynamicInputDim

        Args:
            pretrained (bool, optional): indicates whether to use pretrained values. Defaults to True.
            input_dim (int, optional): number of input channels. Defaults to 4.
            output_dim (int, optional): number of outputs. Defaults to 1.
            training_type (string, optional): fully_trainable or feature_extractor, defines the freezing of layers
        """
        super().__init__()
        weights = 'DEFAULT' if pretrained else None
        self.resnet_model = models.resnet34(weights=weights)
        for param in self.resnet_model.parameters():
            param.requires_grad = False
        self.input_conv_layer = nn.Conv2d(in_channels=input_dim, out_channels=64, kernel_size=(7,7), stride=(2,2), padding=(3,3), bias = False)
        self.model1 = nn.Sequential(*list(self.resnet_model.children())[1:-2])
        self.fully_connected_layer = nn.Sequential(nn.Flatten(),
                                                   nn.Linear(32768, 2048, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(2048, 1024, bias=True),
                                                   nn.ReLU(),
                                                   nn.Linear(1024, output_dim, bias=True))
        
        if pretrained == False: # init weights using xavier initialization
            for model_part in [self.input_conv_layer, self.fully_connected_layer,  self.model1]:
                print("Pretrained: false, using xavier")
                for lay in model_part.modules():
                    if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                        init.xavier_uniform_(lay.weight)

        if training_type == 'fully_trainable': # all params are trainable
            print('fully_trainable')
            for model_part in [self.input_conv_layer, self.fully_connected_layer,  self.model1]:
                for param in model_part.parameters():
                    param.requires_grad = True

        elif training_type == 'feature_extractor': # freeze the resnet backbone
            print('resnet freeze')
            for param in self.regression_model.parameters():
                param.requires_grad = False
            for param in self.regression_model.fc.parameters():
                param.requires_grad = True
            for param in self.regression_model.conv1.parameters():
                param.requires_grad = True                

    def forward(self, x):
        x = self.input_conv_layer(x)
        x = self.model1(x)
        out = self.fully_connected_layer(x)
        return out


class Resnet34RGBchannels(nn.Module):
    """Resnet neural net for regreesion for rgb images

    This neural net accepts images of size 256x256 with 3 channels
    and the output classification layer is replaced by fully connected layer with 3 layers with possibility to set arbitrary number of outputs
    also the net can be modified as a feature exctractor or fullly trainable network.
    The input layer is not modified
    Purpose of this network is to fully use the pretrained weights, which were pretrained on imagenet dataset which contains RGB 3 channels images 

    """
    def __init__(self, pretrained = False, input_dim = 3 , output_dim = 1, training_type = "fully_trainable"):
        """init method for Resnet34RGBchannels

        Args:
            pretrained (bool, optional): indicates whether to use pretrained values. Defaults to True.
            input_dim (int, optional): not used it is hard wired to 3 channels
            output_dim (int, optional): number of outputs. Defaults to 1.
            training_type (string, optional): fully_trainable or feature_extractor, defines the freezing of layers
        """
        super().__init__()
        weights = 'DEFAULT' if pretrained else None
        model = models.resnet34(weights=weights)
        self.regression_model = torch.nn.Sequential(*(list(model.children())[:-2]))
        self.regression_model.fc = torch.nn.Sequential(torch.nn.Flatten(),
                                                torch.nn.Linear(32768 , 2048, bias=True),
                                                torch.nn.ReLU(),
                                                torch.nn.Linear(2048, 1024, bias=True),
                                                torch.nn.ReLU(),
                                                torch.nn.Linear(1024, output_dim, bias=True)) # replace FC layer to regression layer

        if pretrained == False: # init weights using xavier initialization
            print("Pretrained: false, using xavier")
            for lay in self.regression_model.modules():
                if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                    init.xavier_uniform_(lay.weight)
        else:
            if training_type == 'fully_trainable': # all params are trainable
                print('fully_trainable')
                for param in self.regression_model.parameters():
                    param.requires_grad = True
            elif training_type == 'feature_extractor': # freeze the resnet backbone
                print('resnet freeze')
                for param in self.regression_model.parameters():
                    param.requires_grad = False
                for param in self.regression_model.fc.parameters():
                    param.requires_grad = True
                
    def forward(self, x):
        return self.regression_model(x)