from scipy.stats import spearmanr
from scipy.stats import pearsonr
import numpy as np

def computeSpearman(x, y):
    """
    Compute the Spearman rank correlation coefficient between two numpy arrays x and y.
    
    Args:
    - x: numpy array of shape (n,n_features)
    - y: numpy array of shape (n,n_features)
    
    Returns:
    - r: Spearman rank correlation coefficient (float)
    """
    n_dimensions = x.shape[1]
    corr_coefficients = np.zeros(n_dimensions)
    for i in range(n_dimensions):
        corr_coefficients[i], _ = pearsonr(x[:,i], y[:,i])

    return corr_coefficients

def computePearson(x, y):
    """
    Compute the Spearman rank correlation coefficient between two numpy arrays x and y.
    
    Args:
    - x: numpy array of shape (n,)
    - y: numpy array of shape (n,)
    
    Returns:
    - r: Spearman rank correlation coefficient (float)
    """
    n_dimensions = x.shape[1]
    corr_coefficients = np.zeros(n_dimensions)
    for i in range(n_dimensions):
        corr_coefficients[i], _ = spearmanr(x[:,i], y[:,i])

    return corr_coefficients
