import numpy as np
import pandas as pd
import os
import json
import torch
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader, random_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from utils import seg_image_resize, ultra_image_resize
from PIL import Image


def create_dataloader(dataset, config):
    """Function to split dataset into training testing and validation datasets and then normalize them and create dataloaders

    Args:
        dataset (torch.Dataset): datase to be splitted
        config (dictionary): experiment configuration dictionary

    Returns:
        torch.DataLoader, torch.DataLoader, torch.DataLoader, normalization parameters: training testing and validation dataloaders and parameters of the specified normalization
    """
    torch.manual_seed(0)
    train_size = config["dataset"]["train_percentage"]
    test_size = config["dataset"]["test_percentage"]
    val_size = config["dataset"]["val_percentage"]
    batch_size = config["hyperparameters"]["batch_size"]
    # split the dataset into train and test datasets
    train_dataset, test_dataset, val_dataset = random_split(
        dataset, [train_size, test_size, val_size]
    )
    # create data loaders for the train and test datasets

    if config["dataset"]["label_normalization"] != "no_normalization":
        scaler = (
            StandardScaler()
            if config["dataset"]["label_normalization"] == "normal_normalization"
            else MinMaxScaler()
        )
        scaler.fit([item[1].tolist() for item in train_dataset])
        train_dataset = DatasetWithNormalizedLabels(
            train_dataset, scaler, config["dataset"]["transforms"]
        )
        test_dataset = DatasetWithNormalizedLabels(
            test_dataset, scaler, config["dataset"]["transforms"]
        )
        val_dataset = DatasetWithNormalizedLabels(
            val_dataset, scaler, config["dataset"]["transforms"]
        )

    if config["dataset"]["label_normalization"] == "normal_normalization":
        training_norm_params = {
            "mean": scaler.mean_.tolist(),
            "std": scaler.scale_.tolist(), 
        }
    elif config["dataset"]["label_normalization"] == "minmax_normalization":
        training_norm_params = {
            "min": scaler.data_min_.tolist(),
            "max": scaler.data_max_.tolist() 
        }
    else:
        training_norm_params = None
        scaler = (
            StandardScaler()
            if config["dataset"]["label_normalization"] == "normal_normalization"
            else MinMaxScaler()
        )

    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, shuffle=False)
    val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)
    return train_loader, test_loader, val_loader, training_norm_params


def load_synthetic_segmentations(N, dataset_name):
    """Function to load synthetic segmentations from npy files to ndarray

    Args:
        N (int): maximal number of images to be loaded
        dataset_name (string): name of the dataset folder to be loaded

    Returns:
        ndarray: 4d ndarray containg segmentations (N,W,H,3)
    """
    path = os.path.join(
        "..", "data", "synthetic_pictures", dataset_name, "segmented_images"
    )
    seg_files = os.listdir(path)
    if N > len(seg_files):
        print("Error: The number of files is lower than the wanted number")
    segmentaions = []
    for i in range(N):
        f_name = f"image_{i}.npy"
        f_seg_path = os.path.join(path, f_name)
        if os.path.exists(f_seg_path):
            segmentaion = np.load(f_seg_path)
            segmentaions.append(segmentaion)
        else:
            print(f"ERROR The file '{f_seg_path}' does not exist.")
    return np.stack(segmentaions)


def load_synthetic_images(N, dataset_name, expand_grayscale=False):
    """Function to load synthetic random images from npy files to ndarray

    Args:
        N (int): maximal number of images to be loaded
        dataset_name (string): name of the dataset folder to be loaded

    Returns:
        ndarray: 3d ndarray containg random images (N,W,H)
    """
    path = os.path.join(
        "..", "data", "synthetic_pictures", dataset_name, "random_images"
    )
    img_files = os.listdir(path)
    if N > len(img_files):
        print("Error: The number of files is lower than the wanted number")
    images = []
    for i in range(N):
        f_name = f"image_{i}.npy"
        f_img_path = os.path.join(path, f_name)
        if os.path.exists(f_img_path):
            image = np.load(f_img_path)
            images.append(image)
        else:
            print(f"ERROR The file '{f_img_path}' does not exist.")
    images = np.stack(images)
    if expand_grayscale:
        images = np.repeat(images[..., np.newaxis], 3, axis=-1)
    else:
        images = np.expand_dims(images, axis=-1)
    return images


def load_df_labels(
    N, dataset_name, selected_features, label_normalization="no_normalization"
):
    """Function to load labels from already prepared labels dataframe

    Args:
        N (int): maximal number of images to be loaded
        dataset_name (string): name of the dataset folder to be loaded
        selected_features (list(Strings)): list of features which labels should be loaded
        label_normalization (str, optional): specifier which label normalization is to be loaded. Defaults to "no_normalization".

    Returns:
        ndarray: 2d array of labels for N rows and selected features4
    """
    path = os.path.join(
        "..",
        "data",
        "synthetic_pictures",
        dataset_name,
        "labels",
        f"{label_normalization}.csv",
    )
    df = pd.read_csv(path)
    subset_df = df.loc[: N - 1, selected_features]
    subset_array = subset_df.to_numpy()
    return subset_array


def load_raw_labels(N, dataset_name, selected_features):
    """Function to load labels from raw jsons

    Args:
        N (int): maximal number of images to be loaded
        dataset_name (string): name of the dataset folder to be loaded
        selected_features (list(Strings)): list of features which labels should be loaded

    Returns:
         ndarray: 2d array of labels for N rows and selected features4
    """
    path = os.path.join(
        "..", "data", "synthetic_pictures", dataset_name, "labels", "raw_labels"
    )
    labels_np = np.empty((len(N), len(selected_features)))
    for i in range(N):
        l_name = f"image_{i}.json"
        f_lab_path = os.path.join(path, l_name)

        if os.path.exists(f_lab_path):
            with open(f_lab_path, "r") as f:
                label_dict = json.load(f)
            labels_np[i] = [label_dict[feature] for feature in selected_features]
        else:
            print(f"ERROR The file '{f_lab_path}' does not exist.")
    return labels_np


def get_labels_from_row(row, features):
    """Util function to get labels from row into numpy array

    Args:
        row (dataframe.row): row from which features should be extracted
        features (List(String)): List of features

    Returns:
        ndarray: labbels as numpy array
    """
    labels = [row[feature] for feature in features]
    return np.array(labels).astype("float32")


def load_and_resize_conc_images_from_labels(
    seg_path_prog,
    seg_path_stable,
    ultra_path_prog,
    ultra_path_stable,
    labels,
    N=10000,
    features=None,
):
    """Function that takes labels from dataframe and creates ultrasound and segmentation image dataset

    This function is used with labels in csv formats for example the created labels of geometrical parameters or data_with_attribuites.csv which contains names of the images and their features

    Args:
        seg_path_prog (path): path to proggresive segmentations
        seg_path_stable (path): path to stable segmentations
        ultra_path_prog (path): path to proggresive ultrasound images
        ultra_path_stable (path): path to stable ultrasound images
        labels (pd.dataframe): dataframe with labels and name of the pictures
        N (int, optional): maximum number of images. Defaults to 10000.
        features (list(string), optional): List of features to be used. Defaults to None.

    Returns:
        list(tuple(ndarray,ndarray,ndarray)): list of tuples which containns - resized images, resized segmentations, labels

    """
    data = []
    i = 0
    for _, row in labels.iterrows():
        if i >= N:
            break
        file_name = row["name"]
        f_seg_path = os.path.join(
            seg_path_stable if row["type"] != "progressive" else seg_path_prog,
            file_name,
        )
        f_ultra_path = os.path.join(
            ultra_path_stable if row["type"] != "progressive" else ultra_path_prog,
            file_name,
        )

        if os.path.exists(f_seg_path) and os.path.exists(f_ultra_path):
            seg_image = Image.open(f_seg_path)
            seg_image = seg_image.convert("RGB")
            seg_image = seg_image_resize(seg_image)

            ultra_image = Image.open(f_ultra_path)
            ultra_image = ultra_image.convert("RGB")
            ultra_image = ultra_image_resize(ultra_image)

            labels_np = get_labels_from_row(row, features)
            data.append((seg_image, ultra_image, labels_np))
            i += 1
        else:
            print(f"{f_seg_path} or {f_ultra_path} do not exists")

    return data


def load_and_resize_segmentations_from_labels(
    img_path_prog,
    img_path_stable,
    labels,
    N=10000,
    features=None,
):
    """Function that takes labels from dataframe and creates segmentation dataset

    This function is used with labels in csv formats for example the created labels of geometrical parameters or data_with_attribuites.csv which contains names of the images and their features

    Args:
        img_path_prog (path): path to proggresive segmentations
        seg_path_stable (path): path to stable segmentations
        labels (pd.dataframe): dataframe with labels and name of the pictures
        N (int, optional): maximum number of images. Defaults to 10000.
        features (list(string), optional): List of features to be used. Defaults to None.

    Returns:
        list(tuple(ndarray,ndarray)): list of tuples which containns - resized segmentations, labels

    """
    data = []
    i = 0
    for _, row in labels.iterrows():
        if i >= N:
            break
        file_name = row["name"]
        f_path = os.path.join(
            img_path_stable if row["type"] != "progressive" else img_path_prog,
            file_name,
        )
        if os.path.exists(f_path):
            image = Image.open(f_path)
            image = image.convert("RGB")
            resized_image = seg_image_resize(image)
            labels_np = get_labels_from_row(row, features)
            data.append((resized_image, labels_np))
            i += 1
        else:
            print(f"{f_path} do not exists")

    return data


def load_and_resize_ultra_images_from_labels(
    img_path_prog,
    img_path_stable,
    labels,
    N=10000,
    features=None,
):
    """Function that takes labels from dataframe and creates ultrasound image dataset

    This function is used with labels in csv formats for example the created labels of geometrical parameters or data_with_attribuites.csv which contains names of the images and their features

    Args:
        ultra_path_prog (path): path to proggresive ultrasound images
        ultra_path_stable (path): path to stable ultrasound images
        labels (pd.dataframe): dataframe with labels and name of the pictures
        N (int, optional): maximum number of images. Defaults to 10000.
        features (list(string), optional): List of features to be used. Defaults to None.

    Returns:
        list(tuple(ndarray,ndarray)): list of tuples which containns - resized images, labels

    """
    data = []
    i = 0
    for _, row in labels.iterrows():
        if i >= N:
            break
        file_name = row["name"]
        f_path = os.path.join(
            img_path_stable if row["type"] != "progressive" else img_path_prog,
            file_name,
        )
        if os.path.exists(f_path):
            image = Image.open(f_path)
            image = image.convert("RGB")
            image = ultra_image_resize(image)
            labels_np = get_labels_from_row(row, features)
            data.append((image, labels_np))
            i += 1
        else:
            print(f"{f_path} do not exists")

    return data


def get_dataset(config):
    """Main orchestraing function whichh prepares dataset based on config informations

    This function crates datase based on config, possible options are
    -synthetic datasets which  loads labels, and images
    -real dataset loads labels first and then from them decide which images to load

    Args:
        config (dictionary): config dictionary

    Returns:
        torch.util.data.Dataset: Initialized Dataset class with data
    """
    dataset_config = config["dataset"]
    dataset_size = dataset_config["dataset_size"]

    segmentation, images, labels, dataset = [None for _ in range(4)]

    # loading synthetic pictures
    if dataset_config["images_type"] == "synth":
        dataset_name = dataset_config["dataset_name"]
        if dataset_config["include_segmentations"]:
            segmentation = load_synthetic_segmentations(dataset_size, dataset_name)
        if dataset_config["include_images"]:
            images = load_synthetic_images(
                dataset_size,
                dataset_name,
                expand_grayscale=dataset_config["expand_grayscale"],
            )
        labels = load_df_labels(
            dataset_size,
            dataset_name,
            config["predicted_features"],
            dataset_config["label_normalization"],
        )

        dataset = SyntheticDataset(images, segmentation, labels)

    # loading real datasets
    else:
        # deducing paths
        data_path = dataset_config["path"]
        labels_path = os.path.join("..", "data", "real_data", "labels")
        is_transversal = (
            True if "trans" in dataset_config["image_orientation"] else False
        )
        orientation_string = "Trans" if is_transversal else "Long"
        labels_path = os.path.join("..", "data", "real_data", "labels")
        if not config["dataset"]["real_labels"]:
            labels_filename = f"resized_segmentation_{'transversal' if is_transversal else 'longitudal'}_labels.csv"
        else:
            labels_filename = f"resized_annotations_{'transversal' if is_transversal else 'longitudal'}.csv"
        ultra_path_prog = os.path.join(
            data_path, "data", "progressive", orientation_string
        )
        ultra_path_stable = os.path.join(
            data_path, "data", "stable", orientation_string
        )
        seg_path_prog = os.path.join(
            data_path, "masks_resized", "progressive", orientation_string
        )
        seg_path_stable = os.path.join(
            data_path, "masks_resized", "stable", orientation_string
        )

        # loading labels
        labels = pd.read_csv(os.path.join(labels_path, labels_filename))

        # creating appropriate dataset
        if (
            config["dataset"]["include_segmentations"]
            and config["dataset"]["include_images"]
        ):
            data = load_and_resize_conc_images_from_labels(
                seg_path_prog,
                seg_path_stable,
                ultra_path_prog,
                ultra_path_stable,
                labels,
                dataset_config["dataset_size"],
                features=config["predicted_features"],
            )
            dataset = RealConcDataset(data)
        elif config["dataset"]["include_segmentations"]:
            data = load_and_resize_segmentations_from_labels(
                seg_path_prog,
                seg_path_stable,
                labels,
                dataset_config["dataset_size"],
                features=config["predicted_features"],
            )
            dataset = RealSegmentationDataset(data)
        elif config["dataset"]["include_images"]:
            data = load_and_resize_ultra_images_from_labels(
                ultra_path_prog,
                ultra_path_stable,
                labels,
                dataset_config["dataset_size"],
                features=config["predicted_features"],
            )
            dataset = RealUltrasoundDataset(data)
        else:
            print("ERROR NOT IMAGES NOR SEGMENTATIONS")
    return dataset


class DatasetWithNormalizedLabels(Dataset):
    """Dataset class which takes arbitrary dataset normalize labels and performs transformations

    Attributes:
        dataset (torch.Dataset): source dataset
        transform (torch.transform): composed transformations to be computed
        labels (tensor): normalized labels
    """

    def __init__(self, dataset, scaler, transform):
        """Init function

        Args:
            dataset (dataset): source dataset
            scaler (scaler): scaler to be used for normalization
            transform (bool): bool signalizing whether transformations should be done
        """
        self.dataset = dataset
        labels = [item[1].tolist() for item in self.dataset]
        self.labels = torch.tensor(scaler.transform(labels))
        if transform:
            self.transform = transforms.Compose(
                [
                    transforms.RandomHorizontalFlip(p=0.5),
                    transforms.RandomVerticalFlip(p=0.5),
                ]
            )
        else:
            self.transform = transforms.Compose([])

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        return self.transform(self.dataset[idx][0]).float(), self.labels[idx].float()


class RealConcDataset(Dataset):
    """Dataset class for concatenated images

    Attributes:
        x_data (torch.tensor): segmentations and ultrasound images concatenated as tensor with 6 channels
        y_data (torch.tensor): labels as tensors
    """

    def __init__(self, data_list):
        """Init function

        Args:
            data_list (list(tuple(ndarray, ndarray, ndarray))): list of tuples including segmnentation images, ultrasound images and labels.
        """
        segimg = torch.from_numpy(np.array([item[0] for item in data_list])) / 255
        ultraimg = torch.from_numpy(np.array([item[1] for item in data_list]))
        self.x_data = torch.cat((segimg, ultraimg), dim=3)
        self.y_data = torch.from_numpy(np.array([item[2] for item in data_list]))
        self.x_data = self.x_data.permute(0, 3, 1, 2).float()

    def __len__(self):
        return len(self.x_data)

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


class RealUltrasoundDataset(Dataset):
    """Dataset class for ultrasound images

    Attributes:
        x_data (torch.tensor): ultrasouns images as tensor
        y_data (torch.tensor): labels as tensors
    """

    def __init__(self, data_list):
        """Init function

        Args:
            data_list (list(tuple(ndarray, ndarray))): list of tuples including ultrasound images and labels.
        """
        self.x_data = torch.from_numpy(np.array([item[0] for item in data_list]))
        self.y_data = torch.from_numpy(np.array([item[1] for item in data_list]))
        self.x_data = self.x_data.permute(0, 3, 1, 2).float()

    def __len__(self):
        return len(self.x_data)

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


class RealSegmentationDataset(Dataset):
    """Dataset class for segmentations images

    Attributes:
        x_data (torch.tensor): segmentation images as tensor
        y_data (torch.tensor): labels as tensors
    """

    def __init__(self, data_list):
        """Init function

        Args:
            data_list (list(tuple(ndarray, ndarray))): list of tuples including segmentation images and labels.
        """
        self.x_data = torch.from_numpy(np.array([item[0] for item in data_list]))
        self.y_data = torch.from_numpy(np.array([item[1] for item in data_list]))
        self.x_data = self.x_data.permute(0, 3, 1, 2).float() / 255

    def __len__(self):
        return len(self.x_data)

    def __getitem__(self, idx):
        return self.x_data[idx], self.y_data[idx]


class SyntheticDataset(Dataset):
    """Dataset class for synthetic images

    Can be initialized to have segmentations, 1channel ultrasounds, 3channel ultrasounds, 4channels concatenated or 6channels concatenated

    Attributes:
        x_data (torch.tensor): image data as tensor
        y_data (torch.tensor): labels as tensors
    """

    def __init__(self, images, segmentations, labels):
        """initalize class of synthetic imahges, the type is defined by which data is present

        Args:
            images (ndarray): random "ultrasound" images
            segmentations (ndarray): segmentation images
            labels (ndarray): labels
        """

        if segmentations is not None and images is not None:  # case for concatenated
            seg_tensor = torch.from_numpy(segmentations)
            img_tensor = torch.from_numpy(images)
            self.x_data = torch.cat((seg_tensor, img_tensor), dim=3)
        elif segmentations is not None:  # case for segmentations
            seg_tensor = torch.from_numpy(segmentations)
            self.x_data = seg_tensor
        elif images is not None:  # case for random images
            img_tensor = torch.from_numpy(images)
            self.x_data = img_tensor
        else:
            print("ERROR load_segmentation and load_images are both false")
            return

        print(f"x_data shape: {self.x_data.shape}")
        self.x_data = self.x_data.permute(0, 3, 1, 2).float()
        self.x_data = self.x_data / 255
        self.y_data = torch.tensor(labels, dtype=torch.float32)
        if len(self.x_data) != len(self.y_data):
            raise ValueError("X and Y has different size")
        self.n_samples = len(self.x_data)

    def __getitem__(self, index):
        return self.x_data[index], self.y_data[index]

    def __len__(self):
        return self.n_samples
