import os
import matplotlib.pyplot as plt
import json
from os.path import join

RESULTS_PATH = join("..", "server_results")
GRAPHS_PATH = join("..", "graphs")


def create_train_validation_error_graph(experiment_name):
    """Funtion to create graph of trainig and validation errors during the training

    Args:
        experiment_name (str): name of the experiment
    """
    path = join(RESULTS_PATH, experiment_name, f"{experiment_name}.json")
    if not os.path.exists(join(GRAPHS_PATH, experiment_name)):
        os.mkdir(join(GRAPHS_PATH, experiment_name))
    with open(path) as json_file:
        data_dict = json.load(json_file)

    if (
        "train_results" in data_dict
        and "validation_loss_history" in data_dict["train_results"]
    ):
        pass
    else:
        return
    t_losses = data_dict["train_results"]["training_loss_history"]
    v_losses = data_dict["train_results"]["validation_loss_history"]
    x = list(range(len(t_losses)))

    plt.plot(x, t_losses, label="Training loss")
    plt.plot(x, v_losses, label="Validation loss")

    plt.legend()
    plt.savefig(join(GRAPHS_PATH, experiment_name, "train_val_loss.png"))
    plt.show()


def create_train_validation_coeffs_graph(experiment_name):
    """Funtion to create graph of pearson and spearman coefficients over the validation iterations

    Args:
        experiment_name (str): name of the experiment
    """
    path = join(RESULTS_PATH, experiment_name, f"{experiment_name}.json")
    if not os.path.exists(join(GRAPHS_PATH, experiment_name)):
        os.mkdir(join(GRAPHS_PATH, experiment_name))
    with open(path) as json_file:
        data_dict = json.load(json_file)
    if "predicted_features" not in data_dict:
        features = "proportion plaque to vein area"
    else:
        features = data_dict["predicted_features"]

    if "train_results" in data_dict and "spearman_hitory" in data_dict["train_results"]:
        pass
    else:
        return

    print(len(features))
    if len(features) == 1:
        spearmans = data_dict["train_results"]["spearman_hitory"]
        pearsons = data_dict["train_results"]["pearson_history"]
        x = list(range(len(spearmans)))
        # Plot the lines
        plt.plot(x, spearmans, label="Spearman coeff")
        plt.plot(x, pearsons, label="Pearson coeff")

        # Add a legend
        plt.legend()
        plt.savefig(
            join(GRAPHS_PATH, experiment_name, f"{features[0]}_coefficients.png")
        )
        # Show the graph
        plt.show()
    else:
        spearmans = []
        for idx, feature in enumerate(features):
            spearmans = [
                sublist[idx]
                for sublist in data_dict["train_results"]["spearman_hitory"]
            ]
            pearsons = [
                sublist[idx]
                for sublist in data_dict["train_results"]["pearson_history"]
            ]
            x = list(range(len(spearmans)))
            # Plot the lines
            plt.plot(x, spearmans, label="Spearman coeff")
            plt.plot(x, pearsons, label="Pearson coeff")

            # Add a legend
            plt.legend()
            plt.title(feature)
            plt.savefig(
                join(GRAPHS_PATH, experiment_name, f"{features[idx]}_coefficients.png")
            )
            # Show the graph
            plt.show()


def generate_graphs(experiment_name):
    print(experiment_name)
    if os.path.exists(join(RESULTS_PATH, experiment_name, f"{experiment_name}.json")):
        create_train_validation_error_graph(experiment_name)
        create_train_validation_coeffs_graph(experiment_name)
    else:
        print("not existing")


# for experiment in os.listdir(RESULTS_PATH):
#     generate_graphs(experiment)
# generate_graphs("resnet34_segmentation_not_pretrained_trainable")
