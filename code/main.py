import sys
import argparse
import time
import shutil
import os
import json
import numpy as np
import torch
import datasets as ds
from training import test_multi_input_model, train_multi_input_model
import config

CONFIG = config.RILSTrans_segmentation_resnet

RESULTS_PATH = config.RESULTS_PATH
MODELS_PATH = config.MODELS_PATH
DEVICE_NUMBER = config.DEVICE_NUMBER


def run_experiment(device, runtime_config, train=True, test=True):
    """function that orchestrate the whole experiment

    This function prepares file structure, initialize models, possibly train and test models and save the results

    Args:
        device (torch.device): device on which the experiment is run
        runtime_config (dict): configuration from config module, dictionary specifying the experiment
        train (bool, optional): flag whether to run training part. Defaults to True.
        test (bool, optional): flag whether to run testing part. Defaults to True.
    """
    
    # ----------------- Setting the experiment file structure -----------------
    results_folder_path = os.path.join(RESULTS_PATH, runtime_config["name"])
    model_path = os.path.join(MODELS_PATH, f'{runtime_config["model_name"]}.pt')

    if train and os.path.isfile(model_path):
        response = input(
            f'MODEL <{runtime_config["model_name"]}> ALREADY EXISTS - Do you want to continue? (y/n)'
        )
        if response.lower() == "n":
            print("Stopping script...")
            return
    if not train and not os.path.isfile(model_path):
        print("ERROR: MODEL DONT EXISTS - PLEASE TRAIN THE MODEL")
        return
    if os.path.exists(results_folder_path):
        response = input(
            f'EXPERIMENT <{runtime_config["name"]}> ALERADY EXISTS - Do you want to continue? (y/n)'
        )
        if response.lower() == "n":
            print("Stopping script...")
            return
        shutil.rmtree(results_folder_path)

    os.mkdir(results_folder_path)
    os.mkdir(os.path.join(results_folder_path, "validation_results"))

    start_time_dataset = time.time()
    print(
        f'#################     EXPERIMENT: {runtime_config["name"]}      #################'
    )

    # ----------------- Dataset Preparation -----------------
    print(" -Dataset Preparation")
    dataset = ds.get_dataset(runtime_config)
    runtime_config["dataset_size"] = len(dataset)

    train_loader, test_loader, val_loader, runtime_config["normalization_parameters"] = ds.create_dataloader(
        dataset, runtime_config
    )
    print(
        f"train: {len(train_loader)}  test: {len(test_loader)}  val: {len(val_loader)}"
    )
    print(f"     Time elapsed - {(time.time() - start_time_dataset):.2f} s")

    # ----------------- Model Initialization -----------------
    start_time_model_init = time.time()
    print(" -Model Initialization")

    input_dim = dataset[0][0].shape[0]
    print(f"input_dim {input_dim}")

    model = runtime_config["model_function"](
        pretrained=runtime_config["pretrained"],
        input_dim=input_dim,
        output_dim=len(runtime_config["predicted_features"]),
        training_type=runtime_config["training_type"],
    )

    if torch.cuda.is_available():
        model = model.to(device)
    else:
        print("WARING IF ON SERVER torch.cuda is not available")

    print(f"     Time elapsed - {(time.time() - start_time_model_init):.2f} s")

    if train:
        # ----------------- Model Training -----------------
        start_time_training = time.time()
        print(f" -Model Training")
        model_path, train_info = train_multi_input_model(
            model, runtime_config, train_loader, val_loader, device, results_folder_path
        )
        runtime_config["train_results"] = train_info
        print(f"     Time elapsed - {(time.time() - start_time_training):.2f} s")

    if test:
        # ----------------- Model Testing -----------------
        start_time_testing = time.time()
        print(f" -Model Testing")

        best_model = runtime_config["model_function"](
            pretrained=runtime_config["pretrained"],
            input_dim=input_dim,
            output_dim=len(runtime_config["predicted_features"]),
            training_type=runtime_config["training_type"],
        )
        if torch.cuda.is_available():
            best_model = best_model.to(device)
        else:
            print("WARING IF ON SERVER torch.cuda is not available")
        state_dict = torch.load(model_path)
        best_model.load_state_dict(state_dict)

        test_result_metrics = test_multi_input_model(
            best_model, runtime_config, test_loader, results_folder_path, device=device
        )
        runtime_config["test_results"] = test_result_metrics
        print(f"     Time elapsed - {(time.time() - start_time_testing):.2f} s")

    # ----------------- Creating Experiment Info JSON -----------------
    print(" -Creating Results")
    for key, value in runtime_config.items():
        if callable(value):
            runtime_config[key] = value.__name__

    experiments_result_path = os.path.join(
        results_folder_path, f'{runtime_config["name"]}.json'
    )
    with open(experiments_result_path, "w") as f:
        json.dump(runtime_config, f)
    print("EXPERIMENT DONE")


if __name__ == "__main__":
    """Main function to run experiments

    This is the main driver function which sets up server gpus and run experiment based on the CONFIG variable

    """
    # ----------------- SETTING UP DEVICE -----------------
    parser = argparse.ArgumentParser()
    parser.add_argument("--device", type=int, help="number of CUDA device")
    args = parser.parse_args()
    if args.device or args.device == 0:
        DEVICE_NUMBER = args.device
        print(f"DEVICE NUMBER is set to {args.device}")
    else:
        print("No number was provided. Please Provide number")
        sys.exit()

    if torch.cuda.is_available():
        print("CUDA IS AVAILABLE, DEVICE NUMBER {}".format(DEVICE_NUMBER))
        DEVICE = torch.device(DEVICE_NUMBER)
        torch.cuda.set_device(DEVICE_NUMBER)
    else:
        print("NO CUDA IS AVAILABLE, TRAINING ON CPU")
        DEVICE = torch.device("cpu")

    # ----------------- experiment 1 -----------------
    run_experiment(DEVICE, CONFIG, train=True, test=True)
