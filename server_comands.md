cd ../../datagrid/personal/stejson3
nvidia-smi

# ML LOADING

ml Python/3.9.6-GCCcore-11.2.0
ml PyTorch/2.0.0-foss-2022a-CUDA-11.7.0
ml torchvision/0.15.1-foss-2022a-CUDA-11.7.0
ml scikit-learn/1.1.2-foss-2022a
ml OpenCV/4.7.0-foss-2022a-CUDA-11.7.0-contrib
ml scikit-image/0.19.3-foss-2022a

# CONDA 
ml Anaconda3
conda activate /datagrid/personal/stejson3/envs/dp2

# OTHER
export PATH="/home.stud/stejson3:$PATH"