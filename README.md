# Semestral Project

This directory contains source codes and results of the experiments for my semestral project "Odhad parametrů aterosklerotických plátů z ultrazvukových obrazů"

## Instalation

### server
On the cmp server all the code data and results are in folder /datagrid/personal/stejson3 
to run the code after logging to the one of the GPU servers follow these steps:


```bash
cd /datagrid/personal/stejson3/code

# module instalation
ml Python/3.9.6-GCCcore-11.2.0
ml PyTorch/2.0.0-foss-2022a-CUDA-11.7.0
ml torchvision/0.15.1-foss-2022a-CUDA-11.7.0
ml scikit-learn/1.1.2-foss-2022a
ml OpenCV/4.7.0-foss-2022a-CUDA-11.7.0-contrib
ml scikit-image/0.19.3-foss-2022a
```

### local

```bash

conda activate [environment name]
conda install --file requirements.txt
conda python setup_directories.py
```

To be able to run the training of the models please fill the data directory with appropriate data as shown below:

```
data
├───real_data
│   ├───data <- cropped data from "/mnt/bio/ArteryPlaque/in_vivo_platy_segmentace"
│   ├───labels <- annotaions created by automatic script at "/datagrid/personal/stejson3/data/real_data/labels"
│   └───masks_resized <- segmentations from "/mnt/bio/ArteryPlaque/in_vivo_platy_segmentace"
└───synthetic_pictures
```
To generate synthetic data please run 
```
python code/run_data_generation.py
```

It creates 5000 transverse and 10000 longitudinal images in the synthetic_pictures folder or it can be copied from server as shown below

```
data
├───real_data
└───synthetic_pictures
    ├───longitudal_10000 <- /datagrid/personal/stejson3/data/synthetic_pictures/longitudal_10000
    └───transversal_5000 <- /datagrid/personal/stejson3/data/synthetic_pictures/transversal_5000
```

## Run

After succesfully executing the instalation the experiments can be run by following these steps.

Set the expriment which should be executed by setting the variable CONFIG in main.py as following

```
CONFIG = config.[experimentName]
```

the possible values for experimentName are:

```
resnet34_segmentation_notpretrained_trainable
resnet34_segmentation_pretrained_trainable
resnet34_segmentation_pretrained_freezed
resnet34_segmentation_pretrained_trainable_flips

synthTrans_segmentation_resnet
synthTrans_ultra1_resnet
synthTrans_ultra3_resnet
synthTrans_conc4_resnet
synthTrans_conc6_resnet

synthLong_segmentation_resnet
synthLong_ultra1_resnet
synthLong_ultra3_resnet
synthLong_conc4_resnet
synthLong_conc6_resnet

RILSTrans_segmentation_resnet
RILSTrans_ultrasound_resnet
RILSTrans_conc_resnet

RILSLong_segmentation_resnet
RILSLong_ultrasound_resnet
RILSLong_conc_resnet

AnnotationsTrans_segmentation_resnet
AnnotationsTrans_ultrasound_resnet
AnnotationsTrans_conc_resnet

AnnotationsLong_segmentation_resnet
AnnotationsLong_ultrasound_resnet
AnnotationsLong_conc_resnet
```

After deciding which experiment to run please run:
```
python main.py --device [Number of free GPU on server]
```

## Creating own experiment

It is possible to create new experiment simply by creating new dictionary in th code/config.py file the dictionary should have following structure:

```
My_new_experiment= {
    "name": "name of the experiment" 
    "description" : "experiment description",
    "model_name": "name of the saved model",
    "training_type": "fully_trainable or fature_extractor to specify the trainable parmas",
    "pretrained": True if pretrained network False if not,
    "model_function": class of the model from the code/models.py file,
    "predicted_features": list of the names of predicted features,
    "total_number_of_epochs": int number of epochs,
    "loss_function": torch.nn loss functiom, 
    "optimizer_function": torch.optim optimizer function,
    "scheduler":{
        "step_size": int step size,
        "gamma": int gama,
    },
    "dataset":{
        "label_normalization": "normal_normalization" or "minmax_normalization" or "no_normalization,  
        "images_type": "real" if real images "synth" if synthetic images,
        "include_segmentations": True if segmentations are part of the data,
        "include_images": True if ultrasound images are part of the data,
        "images_orientation": "trans" if transversal "long" if longitudinal,
        "real_labels": True if labels are taken from doctors annotaions,
        "path": path to the real data,
        "dataset_size": maximum size of the dataset to be taken,
        "train_percentage": percentage of data to be used for training,
        "test_percentage": percentage of data to be used for testing,
        "val_percentage": percentage of data to be used for validating,
        "transforms": True if augmentation should be performed, 

    },
    hyperparameters: {
        "batch_size": batch size,
        "weight_decay":  weight decay,
        "learning_rate": learning rate
    }

}
```

After creating the file in config.py file you can run the experiment by setting the config variable in main 
```
CONFIG = config.My_new_experiment
```

## Project structure

```
├───code
│   ├── compute_metrics.py                     # functions to compute all the defined geometric parameters
│   ├── config.py                              # contains all the configuraion dictionaries
│   ├── datasets.py                            # all the functions to handle data from folder into the dataloaders sutiable for pytorch model 
│   ├── datAugSingleInput.py                   # functions used for generating synthetic transverse images
│   ├── evaluation.py                          # functions for evaluating the predicted values with true labels
│   ├── generate_Synthetic_data.py             # functions to generate synthetic transvrse and longitudinal data
│   ├── main.py                                # main function to run the experiments 
│   ├── models.py                              # function containg the architectures of NN
│   ├── results_analysis.py                    # functions analysing the results stored in json they create graphs anf other analysis
│   ├── run_data_generation.py                 # script that run the process of data generation
│   ├── statistics_computation.py              # contains function to compute Peason and Spearman
│   ├── training.py                            # implementation of training validation and testing loops
│   └── utils.py                               # utility functions, contain functions to resize images and to creating normalized labels
├───data                                       
│   ├───real_data                              
│   │   ├───data                               # folder containing real ultrasound data
│   │   ├───labels                             # folder containing labels for real data
│   │   └───masks_resized                      # folder containing segmentations created by M.Kostelansky
│   └───synthetic_pictures                     
│       ├───longitudal_10000                   # folder containg synthetically created 10000 longitudinal pictures
│       └───transversal_5000                   # folder containg synthetically created 5000 transversepictures
├───graphs                                     # contains graphs
├───models                                     # contains trained models
├───notebooks                                  
└───results                                    # contains saved results
``` 

